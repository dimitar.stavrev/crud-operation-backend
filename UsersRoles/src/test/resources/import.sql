insert into roles(name, tag, removed) values ('Admin', 3, 0);
insert into roles(name, tag, removed) values ('User', 5, 0);
insert into roles(name, tag, removed) values ('Manager', 7, 0);
insert into roles(name, tag, removed) values ('HR', 9, 0);

insert into users(username, password, full_name, age, removed) values ('sasho', 123, 'Aleksandar', 35, 0);
insert into users(username, password, full_name, age, removed) values ('mitko', 123, 'Dimitar', 33, 0);
insert into users(username, password, full_name, age, removed) values ('ico', 123, 'Hristo', 25, 0);
insert into users(username, password, full_name, age, removed) values ('anton', 123, 'Anton', 22, 0);
insert into users(username, password, full_name, age, removed) values ('atilla', 123, 'Atilla', 22, 0);

insert into users_roles(user_id, role_id) values (1, 1);
insert into users_roles(user_id, role_id) values (1, 2);
insert into users_roles(user_id, role_id) values (2, 2);
insert into users_roles(user_id, role_id) values (3, 3);
insert into users_roles(user_id, role_id) values (4, 2);

insert into organisation(name, address, removed) values ('Amazon', 'USA New York', 0);
insert into organisation(name, address, removed) values ('LightSoft', 'Bulgaria Plovdiv', 0);

insert into department(name, employees_count, organisation_id, removed) values ('IT Services', 22, 1, 0);
insert into department(name, employees_count, organisation_id, removed) values ('Sales', 11, 1, 0);
insert into department(name, employees_count, organisation_id, removed) values ('Delivery', 34, 1, 0);

insert into users_departments(user_id, department_id) values (1, 1);
insert into users_departments(user_id, department_id) values (1, 2);
insert into users_departments(user_id, department_id) values (2, 2);
insert into users_departments(user_id, department_id) values (3, 1);
insert into users_departments(user_id, department_id) values (4, 1);


