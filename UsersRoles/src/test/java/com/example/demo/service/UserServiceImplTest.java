package com.example.demo.service;

import com.example.demo.web.model.UserModelDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {


    private final static String USER_NAME = "mitko";
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;


    @Test
    public void testFindAll() {
        List<UserModelDto> models = this.userService.findAll();
        Assert.assertFalse(CollectionUtils.isEmpty(models));
    }


    @Test
    public void testFindByUsername_Exist() {
        UserModelDto byName = userService.findByUsername(USER_NAME);

        Assert.assertEquals(USER_NAME, byName.getUsername());
    }

    @Test
    public void testFindByUsername_DoesNotExist() {
        UserModelDto byName = userService.findByUsername("NotExist");
        Assert.assertNull(byName);
    }


    @Test
    public void testCreateUser_Successfully() {
        UserModelDto user = new UserModelDto();
        user.setUsername("Dimitar");
        user.setPassword("123");
        user.setFullName("Dim Dim");

        userService.create(user);
        UserModelDto byName = userService.findByUsername("Dimitar");
        Assert.assertNotNull(byName);
        Assert.assertNotNull(byName.getId());
        Assert.assertEquals(user.getPassword(), byName.getPassword());
        Assert.assertEquals(user.getFullName(), byName.getFullName());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateUser_ByExistingName() {
        UserModelDto user = new UserModelDto();
        user.setUsername("mitko");
        user.setPassword("123");
        user.setFullName("Dimitar");

        userService.create(user);
    }


    @Test()
    public void testAddUserRoles_Successfully() {

        userService.addUserRoles(1L, 3L);
        UserModelDto freshUser = userService.findById(1L);
        Assert.assertEquals(3, freshUser.getRoles().size());
//        Assert.assertTrue(freshUser.getRoles()
//                .stream()
//                .anyMatch(r -> r.getId().equals(3L)));
        Assert.assertTrue(freshUser.getRoles()
               .stream()
               .anyMatch(r -> r.equals(3L)));
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRemoveUserRoles_Successfully() {

        userService.removeUserRoles(1L,2L);
        UserModelDto freshUser = userService.findById(1L);
        Assert.assertEquals(1, freshUser.getRoles().size());
        Assert.assertTrue(freshUser.getRoles()
                .stream()
                .anyMatch(r -> r.equals(1L)));
    }

    @Test(expected = EntityNotFoundException.class)
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUser_NotExisting() {
        userService.update(1001L, new UserModelDto());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUser_Successfully() {

        UserModelDto dto = userService.findById(1L);
        dto.setUsername("sasho");
        dto.setPassword("123");
        dto.setFullName("Alex");
        userService.update(1L, dto);
        UserModelDto updatedDto = userService.findByUsername("sasho");
        Assert.assertEquals( "sasho", updatedDto.getUsername());
        Assert.assertEquals("123", updatedDto.getPassword());
        Assert.assertEquals("Alex", updatedDto.getFullName());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDeleteUser_Successfully(){

        userService.deleteById(5L);
        final UserModelDto userModelDto = userService.findByUsername("atilla");
        Assert.assertNull(userModelDto);
    }



}
