package com.example.demo.service;

import com.example.demo.web.model.RoleModelDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RoleServiceImplTest {

    private  final static String ROLE_NAME = "User";
    @Autowired
    private RoleService roleService;


    @Test
    public void testFindAll(){
        List<RoleModelDto> models = this.roleService.findAll();
        Assert.assertFalse(CollectionUtils.isEmpty(models));
    }

    @Test
    public void testFindByName_Exist() {
        RoleModelDto byName = roleService.findByName(ROLE_NAME);

        Assert.assertEquals(ROLE_NAME, byName.getName());
    }

    @Test
    public void testFindByName_DoesNotExist() {
        RoleModelDto byName = roleService.findByName("NotExist");
        Assert.assertNull(byName);
    }


    @Test
    public void testCreateRole_Successfully() {
        RoleModelDto role = new RoleModelDto();
        role.setName("TeamLead");
        role.setTag(4);

        roleService.create(role);
        RoleModelDto byName = roleService.findByName("TeamLead");
        Assert.assertNotNull(byName);
        Assert.assertNotNull(byName.getId());
        Assert.assertEquals(role.getTag(), byName.getTag());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateDepartment_ByExistingName() {
        RoleModelDto role = new RoleModelDto();
        role.setName("User");
        role.setTag(4);

        roleService.create(role);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateRole_NotExisting() {
        roleService.update(1001L, new RoleModelDto());
    }


    @Test
    public void testUpdateRole_Successfully() {

        RoleModelDto dto = roleService.findById(1L);
        dto.setName("Admin");
        dto.setTag(10);
        roleService.update(1L, dto);
        RoleModelDto updatedDto = roleService.findByName("Admin");
        Assert.assertEquals( "Admin", updatedDto.getName());
        Assert.assertEquals(10, updatedDto.getTag());
    }

    @Test
    public void testDeleteRole_Successfully(){

        roleService.deleteById(4L);
        final RoleModelDto roleModelDto = roleService.findByName("HR");
        Assert.assertNull(roleModelDto);
    }
}
