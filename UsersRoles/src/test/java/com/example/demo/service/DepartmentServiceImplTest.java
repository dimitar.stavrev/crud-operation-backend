package com.example.demo.service;

import com.example.demo.web.model.DepartmentModelDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import javax.persistence.EntityNotFoundException;
import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class DepartmentServiceImplTest {

    private final static String DEPARTMENT_NAME = "Sales";
    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserService userService;

    @Test
    public void testSelectAll() {
        final List<DepartmentModelDto> models = this.departmentService.findAll();
        Assert.assertFalse(CollectionUtils.isEmpty(models));
    }

    @Test
    public void testFindByName_Exist() {
        DepartmentModelDto byName = departmentService.findByName(DEPARTMENT_NAME);

        Assert.assertEquals(DEPARTMENT_NAME, byName.getName());
    }

    @Test
    public void testFindByName_DoesNotExist() {
        DepartmentModelDto byName = departmentService.findByName("NotExist");
        Assert.assertNull(byName);
    }

    @Test
    public void testCreateDepartment_Successfully() {
        DepartmentModelDto department = new DepartmentModelDto();
        department.setName("Marketing");
        department.setEmployeesCount(43);

        departmentService.create(department);
        DepartmentModelDto byName = departmentService.findByName("Marketing");
        Assert.assertNotNull(byName);
        Assert.assertNotNull(byName.getId());
        Assert.assertEquals(department.getEmployeesCount(), byName.getEmployeesCount());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateDepartment_ByExistingName() {
        DepartmentModelDto department = new DepartmentModelDto();
        department.setName("IT Services");
        department.setEmployeesCount(43);

        departmentService.create(department);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateDepartment_NotExisting() {
        departmentService.update(1001L, new DepartmentModelDto());
    }

    @Test
    public void testUpdateDepartment_Successfully() {
        // 'IT Services', 22, 1);
//        Department d = repository.findById(1L).get();
//        d.setName("IT Services");
//        d.setEmployeesCount(24);
//        final DepartmentModelDto dto = new DepartmentModelDto();
//        d.toDto(dto);

        DepartmentModelDto dto = departmentService.findById(1L);
        dto.setName("IT Services");
        dto.setEmployeesCount(24);
        departmentService.update(1L, dto);
        DepartmentModelDto updatedDto = departmentService.findByName("IT Services");
        Assert.assertEquals( "IT Services", updatedDto.getName());
        Assert.assertEquals(24, updatedDto.getEmployeesCount());
    }

    @Test
    public void testDeleteDepartment_Successfully(){

        departmentService.deleteById(3L);
        final DepartmentModelDto departmentModelDto = departmentService.findByName("Delivery");
        Assert.assertNull(departmentModelDto);
    }
}
