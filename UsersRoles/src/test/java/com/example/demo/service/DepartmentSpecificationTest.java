package com.example.demo.service;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.model.Department;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.specification.DepartmentSpecifications;
import com.example.demo.util.Operation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class DepartmentSpecificationTest {

    @Autowired
    private DepartmentRepository repository;

    @Test
    public void testAgeLessThanAndNameContainsLetters(){
        final DepartmentFilter filter = new DepartmentFilter();
        filter.setName("a");
        filter.setOperation(Operation.LESS_THAN);
        filter.setEmployeesCount(25);
        List<Department> departments = repository.findAll(new DepartmentSpecifications(filter));
        Assert.assertNotNull(departments);
        Assert.assertEquals(1, departments.size());
    }

    @Test
    public void testAgeGreaterThanAndNameContainsLetters(){
        final DepartmentFilter filter = new DepartmentFilter();
        filter.setName("r");
        filter.setOperation(Operation.GREATER_THAN);
        filter.setEmployeesCount(20);
        List<Department> departments = repository.findAll(new DepartmentSpecifications(filter));
        Assert.assertNotNull(departments);
        Assert.assertEquals(2, departments.size());
    }
}
