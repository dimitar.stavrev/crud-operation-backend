package com.example.demo.service;

import com.example.demo.filter.UserFilter;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.specification.UserSpecifications;
import com.example.demo.util.Operation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserSpecificationTest {

    @Autowired
    private UserRepository userRepository;


    @Test
    public void testLessThanAgeAndUsernameContainsLetters() {
        final UserFilter filter = new UserFilter();
        filter.setAge(30);
        filter.setAgeOperation(Operation.LESS_THAN);
        filter.setUsername("t");
        List<User> users = userRepository.findAll(new UserSpecifications(filter));
        assertNotNull(users);
        assertEquals(2, users.size());
    }

    @Test
    public void testGreaterThanAgeAndUsernameContainsLetters() {
        final UserFilter filter = new UserFilter();
        filter.setAge(30);
        filter.setAgeOperation(Operation.GREATER_THAN);
        filter.setUsername("o");
        List<User> users = userRepository.findAll(new UserSpecifications(filter));
        assertNotNull(users);
        assertEquals(2, users.size());
    }



//    @Test
//    public void givenPartialFirst_whenGettingListOfUsers_thenCorrect() {
//
//        List<User> users = userRepository.findAll(UserSpecifications.likeFullName("Dim"));
//        assertNotNull(users);
//        assertEquals(1, users.size());
//    }
//
//    @Test
//    public void testGreaterThanAge() {
//
//        List<User> users = userRepository.findAll(UserSpecifications.greaterThanAge(30));
//        assertNotNull(users);
//        assertEquals(2, users.size());
//    }
}
