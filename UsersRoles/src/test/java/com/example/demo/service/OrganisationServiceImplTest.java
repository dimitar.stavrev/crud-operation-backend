package com.example.demo.service;

import com.example.demo.web.model.OrganisationModelDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import javax.persistence.EntityNotFoundException;


@SpringBootTest
@RunWith(SpringRunner.class)
public class OrganisationServiceImplTest {

    private final static String ORGANISATION_NAME = "Amazon";
    @Autowired
    private OrganisationService organisationService;


    @Test
    public void testFindByName_Exist() {
        OrganisationModelDto byName = organisationService.findByName(ORGANISATION_NAME);

        Assert.assertEquals(ORGANISATION_NAME, byName.getName());
    }

    @Test
    public void testFindByName_DoesNotExist() {
        OrganisationModelDto byName = organisationService.findByName("NotExist");
        Assert.assertNull(byName);
    }

    @Test
    public void testCreateOrganisation_Successfully(){
        OrganisationModelDto organisation = new OrganisationModelDto();
        organisation.setName("Light Soft");
        organisation.setAddress("Plovdiv");

        organisationService.create(organisation);
        OrganisationModelDto light_soft = organisationService.findByName("Light Soft");
        Assert.assertNotNull(light_soft);
        Assert.assertNotNull(light_soft.getId());
        Assert.assertEquals(organisation.getAddress(), light_soft.getAddress());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateOrganisation_ByExistingName() {
        OrganisationModelDto organisation = new OrganisationModelDto();
        organisation.setName("Amazon");
        organisation.setAddress("USA NEW YORK");

        organisationService.create(organisation);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateOrganisation_NotExisting() {
        organisationService.update(1001L, new OrganisationModelDto());
    }

    @Test
    public void testUpdateOrganisation_Successfully() {
//        Organisation organisation = repository.findById(1L).get();
//        organisation.setName("Amazon");
//        organisation.setAddress("Bulgaria Plovdiv");
//        final OrganisationModelDto dto = new OrganisationModelDto();
//        organisation.toDto(dto);

        OrganisationModelDto dto = organisationService.findById(1L);
        dto.setName("Amazon");
        dto.setAddress("Bulgaria Plovdiv");
        organisationService.update(1L, dto);
        OrganisationModelDto amazon = organisationService.findByName("Amazon");
        Assert.assertEquals("Amazon", amazon.getName());
        Assert.assertEquals("Bulgaria Plovdiv", amazon.getAddress());
    }

    @Test
    public void testDeleteOrganisation_Successfully(){

        organisationService.deleteById(2L);
        final OrganisationModelDto organisationModelDto = organisationService.findByName("LightSoft");
        Assert.assertNull(organisationModelDto);
    }
}
