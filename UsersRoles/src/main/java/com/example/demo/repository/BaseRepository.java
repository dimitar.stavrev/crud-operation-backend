package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository <E> extends JpaRepository<E,Long> {

    Optional<E> findByNameAndRemoved(String name, boolean removed);
    Optional<E> findByIdAndRemoved(Long id, boolean removed);
    List<E> findByRemoved(boolean removed);


    @Query("UPDATE #{#entityName} e SET e.removed = true where e.id = :id")
    @Modifying
    @Transactional
    void markAsRemoved(@Param("id") final Long id);

}
