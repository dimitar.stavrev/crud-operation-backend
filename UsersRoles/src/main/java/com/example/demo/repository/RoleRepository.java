package com.example.demo.repository;

import com.example.demo.model.Role;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RoleRepository extends BaseRepository<Role> {

    List<Role> findAllByName(String roleName);



}
