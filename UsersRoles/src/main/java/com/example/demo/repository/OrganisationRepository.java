package com.example.demo.repository;

import com.example.demo.model.Organisation;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganisationRepository extends BaseRepository<Organisation> {


}
