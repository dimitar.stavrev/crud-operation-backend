package com.example.demo.service;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.web.model.DepartmentModelDto;

import java.util.List;


public interface DepartmentService extends BaseService<DepartmentModelDto>{

   List<DepartmentModelDto> findByDepartmentNameAndEmployeesCount(DepartmentFilter filter);
}
