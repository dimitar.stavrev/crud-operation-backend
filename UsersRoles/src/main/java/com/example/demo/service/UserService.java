package com.example.demo.service;

import com.example.demo.filter.UserFilter;
import com.example.demo.web.model.UserModelDto;

import java.util.List;


public interface UserService extends BaseService<UserModelDto>{


    void addUserRoles(Long userId, Long roleId);

    void removeUserRoles(Long userId, Long roleId);

    public UserModelDto findByUsername(String username);

    List<UserModelDto> findByUsernameAndAge(UserFilter filter);



}
