package com.example.demo.service;

import com.example.demo.model.BaseEntity;
import com.example.demo.repository.BaseRepository;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;


public abstract class BaseServiceImpl<R extends BaseRepository<E>, E extends BaseEntity<D>, D> implements BaseService<D> {

    protected final R repository;
    private final Class<E> entityClass;
    private final Class<D> dtoClass;

    protected BaseServiceImpl(R repository, Class<E> entityClass, Class<D> dtoClass) {
        this.repository = repository;
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }


    @Override
    public void create(D dto) {
        E entity = null;
        try {
            entity = entityClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        entity.toEntity(dto);

        repository.save(entity);
    }


    @Override
    public D update(Long id, D dto) {
        E entity = repository.findByIdAndRemoved(id, false).orElseThrow(EntityNotFoundException::new);

        entity.toEntity(dto);
        E savedEntity = repository.save(entity);

        D modelDto = null;
        try {
            modelDto = dtoClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        savedEntity.toDto(modelDto);
        return modelDto;
    }

    @Override
    public D findByName(String name) {

        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null!");
        }

        return this.repository.findByNameAndRemoved(name,false)
                .map(entity -> {
                    D dto = null;
                    try {
                        dto = dtoClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                    entity.toDto(dto);
                    return dto;
                })
                .orElse(null);
    }

    @Override
    public D findById(Long id) {

        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null!");
        }
        return this.repository.findByIdAndRemoved(id,false)
                .map(entity -> {
                    D dto = null;
                    try {
                        dto = dtoClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                    entity.toDto(dto);
                    return dto;
                })
                .orElse(null);
    }


    @Override
    public List<D> findAll() {
        return repository
                .findByRemoved(false)
                .stream()
                .map(entity -> {
                    D dto = null;
                    try {
                        dto = dtoClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                    entity.toDto(dto);
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        repository.markAsRemoved(id);
    }


}
