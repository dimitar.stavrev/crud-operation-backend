package com.example.demo.service;

import com.example.demo.model.Organisation;
import com.example.demo.repository.OrganisationRepository;
import com.example.demo.web.model.OrganisationModelDto;
import org.springframework.stereotype.Service;


@Service
public class OrganisationServiceImpl extends BaseServiceImpl<OrganisationRepository, Organisation, OrganisationModelDto> implements OrganisationService {


    public OrganisationServiceImpl(OrganisationRepository organisationRepository) {
        super(organisationRepository, Organisation.class, OrganisationModelDto.class);
    }

}
