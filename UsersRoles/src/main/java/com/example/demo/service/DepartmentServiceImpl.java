package com.example.demo.service;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.model.Department;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.specification.DepartmentConvertSpecifications;
import com.example.demo.web.model.DepartmentModelDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class DepartmentServiceImpl extends BaseServiceImpl<DepartmentRepository, Department, DepartmentModelDto> implements DepartmentService {


    private final DepartmentConvertSpecifications convertSpecifications;
    public DepartmentServiceImpl(DepartmentRepository departmentRepository, DepartmentConvertSpecifications convertSpecifications) {
        super(departmentRepository, Department.class, DepartmentModelDto.class);
        this.convertSpecifications = convertSpecifications;
    }


    @Override
    public List<DepartmentModelDto> findByDepartmentNameAndEmployeesCount(DepartmentFilter filter) {
        return repository.findAll(convertSpecifications.findByFilter(filter))
                .stream()
                .map(department -> {
                    DepartmentModelDto dto = new DepartmentModelDto();
                    department.toDto(dto);
                    return dto;
                })
                .collect(Collectors.toList());
    }
}
