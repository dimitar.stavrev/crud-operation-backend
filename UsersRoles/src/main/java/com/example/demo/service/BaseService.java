package com.example.demo.service;

import java.util.List;

public interface BaseService<D> {

    D findByName(String name);

    D findById(Long id);

    void deleteById(Long id);

    List<D> findAll();

    void create(D dto);

    D update(Long id, D dto);

}
