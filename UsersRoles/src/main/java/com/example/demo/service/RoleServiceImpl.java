package com.example.demo.service;

import com.example.demo.model.Role;
import com.example.demo.repository.RoleRepository;
import com.example.demo.web.model.RoleModelDto;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends BaseServiceImpl< RoleRepository, Role, RoleModelDto> implements RoleService {



    public RoleServiceImpl(RoleRepository roleRepository) {
      super(roleRepository, Role.class, RoleModelDto.class);
    }

}
