package com.example.demo.service;

import com.example.demo.filter.UserFilter;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.specification.UserConvertSpecifications;
import com.example.demo.web.model.UserModelDto;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl extends BaseServiceImpl<UserRepository, User, UserModelDto> implements UserService {

    private final RoleRepository roleRepository;
    private final UserConvertSpecifications convertSpecifications;



    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, UserConvertSpecifications convertSpecifications) {
        super(userRepository, User.class, UserModelDto.class);
        this.roleRepository = roleRepository;
        this.convertSpecifications = convertSpecifications;

    }

    @Override
    public UserModelDto findByUsername(String username) {
        return repository.findByUsernameAndRemoved(username, false)
                .map(entity -> {
                    UserModelDto dto = new UserModelDto();
                    entity.toDto(dto);
                    return dto;
                })
                .orElse(null);
    }

    @Override
    public List<UserModelDto> findByUsernameAndAge(UserFilter filter) {

        return repository.findAll(convertSpecifications.findByFilter(filter))
                .stream()
                .map(user -> {
                    UserModelDto dto = new UserModelDto();
                    user.toDto(dto);
                    return dto;
                })
                .collect(Collectors.toList());
    }



    @Override
    public void addUserRoles(Long userId, Long roleId) {
        User user = repository.findById(userId).orElseThrow(EntityNotFoundException::new);
        Role role = roleRepository.findById(roleId).orElseThrow(EntityNotFoundException::new);
        List<Role> userRoles = user.getRoles();
        userRoles.add(role);
        user.setRoles(userRoles);
        repository.save(user);
    }

    @Override
    public void removeUserRoles(Long userId, Long roleId) {
        User user = repository.findById(userId).orElseThrow(EntityNotFoundException::new);
        Role role = roleRepository.findById(roleId).orElseThrow(EntityNotFoundException::new);
        List<Role> userRoles = user.getRoles();
        userRoles.remove(role);
        user.setRoles(userRoles);
        repository.save(user);
    }

}
