package com.example.demo.util;

public enum Operation {
    EQUAL,
    GREATER_THAN,
    LESS_THAN,
}
