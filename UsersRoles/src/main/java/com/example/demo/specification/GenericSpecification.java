package com.example.demo.specification;

import org.springframework.data.jpa.domain.Specification;


public interface GenericSpecification<E, F> {

     Specification<E> findByFilter(F filter);

}
