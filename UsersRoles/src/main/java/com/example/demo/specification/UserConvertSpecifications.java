package com.example.demo.specification;

import com.example.demo.filter.UserFilter;
import com.example.demo.model.User;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;


@Component
public class UserConvertSpecifications implements GenericSpecification<User, UserFilter>{


    @Override
    public Specification<User> findByFilter(UserFilter filter) {
        return new UserSpecifications(filter);
    }
}
