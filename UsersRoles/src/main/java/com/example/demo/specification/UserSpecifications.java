package com.example.demo.specification;

import com.example.demo.filter.UserFilter;
import com.example.demo.model.User;
import com.example.demo.model.User_;
import org.springframework.data.jpa.domain.Specification;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UserSpecifications implements Specification<User> {
    private final UserFilter filter;

    public UserSpecifications(UserFilter filter) {
        this.filter = filter;
    }



    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        final List<Predicate> predicates = new ArrayList<>();

        if(filter.getUsername() != null) {
            predicates.add(criteriaBuilder.like(root.get(User_.username),"%" + filter.getUsername() + "%"));
        }


        if(filter.getAge() != null) {
            switch (filter.getAgeOperation()) {
                case LESS_THAN: {
                    predicates.add(criteriaBuilder.lessThan(root.get(User_.age), filter.getAge()));
                }
                break;
                case GREATER_THAN: {
                    predicates.add(criteriaBuilder.greaterThan(root.get(User_.age), filter.getAge()));
                }
                break;
                default: {
                    predicates.add(criteriaBuilder.equal(root.get(User_.age), filter.getAge()));
                }
            }
        }
        if(filter.isRemoved() != null){
            predicates.add(criteriaBuilder.equal(root.get(User_.removed), filter.isRemoved()));
        }
        return query.where(predicates.toArray(new Predicate[0]))
                .getGroupRestriction();
    }






    //    public static Specification<User> likeFullName(String fullName){
//        if (fullName == null){
//            return null;
//        }
//        return (root, query, cb) -> cb.like(root.get(User_.NAME), "%" + fullName + "%");
//    }
//
//    public static Specification<User> greaterThanAge(int age){
//        if (age == 0){
//            return null;
//        }
//        return (root, query, cb) -> cb.greaterThan(root.get(User_.AGE), age);
//    }
//
//    public static Specification<User> lessThanAge(int age){
//        if (age == 0){
//            return null;
//        }
//        return (root, query, cb) -> cb.lessThan(root.get(User_.AGE), age);
//    }
}
