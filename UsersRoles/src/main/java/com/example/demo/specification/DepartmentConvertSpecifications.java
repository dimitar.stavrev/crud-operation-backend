package com.example.demo.specification;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.model.Department;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;


@Component
public class DepartmentConvertSpecifications implements GenericSpecification<Department, DepartmentFilter>{


    @Override
    public Specification<Department> findByFilter(DepartmentFilter filter) {
        return new DepartmentSpecifications(filter);
    }
}
