package com.example.demo.specification;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.model.Department;
import com.example.demo.model.Department_;
import org.springframework.data.jpa.domain.Specification;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class DepartmentSpecifications implements Specification<Department> {

    private final DepartmentFilter filter;

    public DepartmentSpecifications(DepartmentFilter filter) {
        this.filter = filter;
    }


    @Override
    public Predicate toPredicate(Root<Department> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        if (filter.getName() != null){
             predicates.add(criteriaBuilder.like(root.get(Department_.NAME), "%" + filter.getName() + "%"));
        }
        if (filter.getEmployeesCount() != null){
            switch (filter.getOperation()){
                case GREATER_THAN:
                    predicates.add(criteriaBuilder.greaterThan(root.get(Department_.employeesCount), filter.getEmployeesCount()));
                    break;
                case LESS_THAN:
                    predicates.add(criteriaBuilder.lessThan(root.get(Department_.employeesCount), filter.getEmployeesCount()));
                    break;
                default:
                    predicates.add(criteriaBuilder.equal(root.get(Department_.employeesCount), filter.getEmployeesCount()));
            }
        }

        if (filter.isRemoved() != null){
            predicates.add(criteriaBuilder.equal(root.get(Department_.removed), filter.isRemoved()));
        }
        return query.where(predicates.toArray(new Predicate[0]))
                .getGroupRestriction();
    }
}
