package com.example.demo.config;

import org.hibernate.collection.spi.PersistentCollection;
import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BaseConfiguration {

    @Bean
    @Scope("prototype")
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
