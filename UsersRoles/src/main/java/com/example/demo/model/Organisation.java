package com.example.demo.model;

import com.example.demo.web.model.OrganisationModelDto;
import org.springframework.util.CollectionUtils;
import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "organisation")
public class Organisation extends BaseEntity<OrganisationModelDto> {

    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "removed", nullable = false)
    private boolean removed = false;

    @OneToMany(mappedBy = "organisation", fetch = FetchType.EAGER, targetEntity = Department.class)
    private List<Department> departments;


    public Organisation() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    @Override
    public void toDto(final OrganisationModelDto dto) {
        if (dto == null) {
            return;
        }
        dto.setId(this.getId());
        dto.setName(this.getName());
        dto.setAddress(this.getAddress());

        if (!CollectionUtils.isEmpty(this.getDepartments())) {
            dto.setDepartments(this.getDepartments()
                    .stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toList()));
        }

//        if (!CollectionUtils.isEmpty(this.getDepartments())) {
//            dto.setDepartments(this.getDepartments()
//                    .stream()
//                    .map(department -> {
//                        final DepartmentModelDto departmentModelDto = new DepartmentModelDto();
//                        department.toDto(departmentModelDto);
//                        department.setOrganisation(null);
//                        return departmentModelDto;
//                    })
//                    .collect(Collectors.toList()));
//        }
    }

    @Override
    public void toEntity(final OrganisationModelDto dto) {
        if (dto == null) {
            return;
        }
        this.setId(dto.getId());
        this.setName(dto.getName());
        this.setAddress(dto.getAddress());

        if (!CollectionUtils.isEmpty(dto.getDepartments())){
            this.setDepartments(dto.getDepartments()
                    .stream()
                    .map(id-> {
                        final Department departmentEntity = new Department();
                        departmentEntity.setId(id);
                        return departmentEntity;
                    })
                    .collect(Collectors.toList()));
        }

//        if (!CollectionUtils.isEmpty(dto.getDepartments())){
//            this.setDepartments(dto.getDepartments()
//                    .stream()
//                    .map(dto-> {
//                        final Department departmentEntity = new Department();
//                        departmentEntity.toEntity(dto);
//                        return departmentEntity;
//                    })
//                    .collect(Collectors.toList()));
//        }
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
