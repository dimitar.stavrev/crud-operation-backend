package com.example.demo.model;

import com.example.demo.web.model.DepartmentModelDto;
import org.springframework.util.CollectionUtils;
import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Table(name = "department")
public class Department extends BaseEntity<DepartmentModelDto> {

    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "employees_count")
    private int employeesCount;

    @Column(name = "removed", nullable = false)
    private boolean removed = false;
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Organisation.class)
    @JoinColumn(name = "organisation_id", referencedColumnName = "id")
    private Organisation organisation;

    @ManyToMany(fetch = FetchType.EAGER, targetEntity = User.class)
    @JoinTable(name = "users_departments",
            joinColumns = {@JoinColumn(name = "department_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private List<User> users;



    public Department() {
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public int getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(int employeesCount) {
        this.employeesCount = employeesCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public void toDto(final DepartmentModelDto dto) {
        if (dto == null ) {
            return;
        }

        dto.setId(this.getId());
        dto.setName(this.getName());
        dto.setEmployeesCount(this.getEmployeesCount());
        if (this.getOrganisation() != null) {
            dto.setOrganisation(this.getOrganisation().getId());
        }
//        if (this.getOrganisation() != null) {
//            final OrganisationModelDto organisationModelDto = new OrganisationModelDto();
//            this.getOrganisation().setDepartments(null);
//            this.getOrganisation().toDto(organisationModelDto);
//            dto.setOrganisation(organisationModelDto);
//        }
        if (!CollectionUtils.isEmpty(this.getUsers())) {
            dto.setUsers(this.getUsers()
                    .stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toList()));
        }
    }

    @Override
    public void toEntity(final DepartmentModelDto dto) {
        if (dto == null) {
            return;
        }
        this.setId(dto.getId());
        this.setName(dto.getName());
        this.setEmployeesCount(dto.getEmployeesCount());
//        if (dto.getOrganisation() != null) {
//            final Organisation organisationEntity = new Organisation();
//            organisationEntity.toEntity(dto.getOrganisation());
//            this.setOrganisation(organisationEntity);
//        }
        if (dto.getOrganisation() != null){
            final Organisation organisationEntity = new Organisation();
            organisationEntity.setId(dto.getOrganisation());
            this.setOrganisation(organisationEntity);
        }
        if (!CollectionUtils.isEmpty(dto.getUsers())) {
            this.setUsers(dto.getUsers().stream()
                    .map(id -> {
                        final User userEntity = new User();
                        userEntity.setId(id);
                        return userEntity;
                    }).collect(Collectors.toList()));
        }
    }
}
