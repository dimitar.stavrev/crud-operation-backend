package com.example.demo.model;

import com.example.demo.web.model.RoleModelDto;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "roles")
public class Role extends BaseEntity<RoleModelDto> {

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "tag")
    private int tag;

    @Column(name = "removed", nullable = false)
    private boolean removed = false;

    public Role() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }


    @Override
    public void toDto(final RoleModelDto dto) {
        if (dto == null) {
            return;
        }
        dto.setId(this.getId());
        dto.setName(this.getName());
        dto.setTag(this.getTag());
    }

    @Override
    public void toEntity(final RoleModelDto dto) {
        if (dto == null) {
            return;
        }
        this.setId(dto.getId());
        this.setName(dto.getName());
        this.setTag(dto.getTag());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;
        Role role = (Role) o;
        return tag == role.tag && Objects.equals(name, role.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, tag);
    }


    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
