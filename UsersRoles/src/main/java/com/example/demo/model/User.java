package com.example.demo.model;

import com.example.demo.web.model.UserModelDto;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.util.CollectionUtils;
import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Table(name = "users")
public class User extends BaseEntity<UserModelDto> {
    @Column(name = "username", nullable = false, unique = true)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "full_name", nullable = false)
    private String name;

    @Column(name = "age", nullable = false)
    private int age;

    @Column(name = "removed", nullable = false)
    private boolean removed = false;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;


    @ManyToMany(fetch = FetchType.EAGER, targetEntity = Department.class)
    @JoinTable(name = "users_departments",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "department_id")})
    private List<Department> departments;


    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    @Override
    public void toDto(final UserModelDto dto) {
        if (dto == null) {
            return;
        }
        dto.setId(this.getId());
        dto.setUsername(this.getUsername());
        dto.setPassword(this.getPassword());
        dto.setFullName(this.getName());
        dto.setAge(this.getAge());
        if (!CollectionUtils.isEmpty(this.getDepartments())){
            dto.setDepartments(this.getDepartments()
                    .stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toList()));
        }

        if (!CollectionUtils.isEmpty(this.getRoles())){
            dto.setRoles(this.getRoles()
                    .stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toList()));
        }
    }

    @Override
    public void toEntity(final UserModelDto dto) {
        if (dto == null) {
            return;
        }
        this.setId(dto.getId());
        this.setUsername(dto.getUsername());
        this.setPassword(dto.getPassword());
        this.setName(dto.getFullName());
        this.setAge(dto.getAge());
        if (!CollectionUtils.isEmpty(dto.getDepartments())){
            this.setDepartments(dto.getDepartments()
                    .stream()
                    .map(id -> {
                        final Department departmentEntity = new Department();
//                        departmentEntity.setUsers(null);
                        departmentEntity.setId(id);
                        return departmentEntity;
                    })
                    .collect(Collectors.toList()));
        }

        if (!CollectionUtils.isEmpty(dto.getRoles())){
            this.setRoles(dto.getRoles()
                    .stream()
                    .map(id -> {
                        Role roleEntity = new Role();
                        roleEntity.setId(id);
                        return roleEntity;
                    })
                    .collect(Collectors.toList()));
        }
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
