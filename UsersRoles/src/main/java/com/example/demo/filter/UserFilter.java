package com.example.demo.filter;

import com.example.demo.util.Operation;

public class UserFilter {
    private String username;
    private Integer age;
    private Operation ageOperation = Operation.EQUAL;
    private Boolean removed = false;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Operation getAgeOperation() {
        return ageOperation;
    }

    public void setAgeOperation(Operation ageOperation) {
        this.ageOperation = ageOperation;
    }


    public Boolean isRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }
}
