package com.example.demo.web.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class RoleModelDto {

    private Long id;
    @Size(min = 3, max = 30)
    private String name;
    @Positive
    private int tag;


    public RoleModelDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }


}
