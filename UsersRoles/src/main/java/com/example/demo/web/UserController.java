package com.example.demo.web;

import com.example.demo.filter.UserFilter;
import com.example.demo.model.User;
import com.example.demo.web.model.UserModelDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RequestMapping(value = "/user",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)

public interface UserController {

    @GetMapping(value = "/read")
    ResponseEntity<UserModelDto> getUserByUsername(@RequestParam String username);

    @GetMapping(value = "/getId/{id}")
    ResponseEntity<UserModelDto> getUserById(@PathVariable Long id);

    @GetMapping(value = "/readAll")
    ResponseEntity<List<UserModelDto>> getAllUsers();

    @PostMapping(value = "/search")
    ResponseEntity<List<UserModelDto>> searchByUsernameAndAge(@RequestBody UserFilter filter);

    @PostMapping(value = "/create")
    ResponseEntity<Void> createUser(@Valid @RequestBody UserModelDto userModelDto);


    @PutMapping("/update/{id}")
    ResponseEntity<UserModelDto> updateUserById( @PathVariable("id") Long id, @RequestBody UserModelDto userModelDto);

    @PutMapping("/addUserRoles/{userId}/{roleId}")
    ResponseEntity<HttpStatus> addUserRoles(@PathVariable("userId") Long userId, @PathVariable("roleId") Long roleId);

    @PutMapping("/removeUserRoles/{userId}/{roleId}")
    ResponseEntity<HttpStatus> removeUserRoles(@PathVariable("userId") Long userId, @PathVariable("roleId") Long roleId);

    @DeleteMapping("/delete/{id}")
    ResponseEntity<HttpStatus> deleteUserById(@PathVariable("id") Long id);
}
