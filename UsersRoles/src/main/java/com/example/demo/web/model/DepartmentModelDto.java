package com.example.demo.web.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

public class DepartmentModelDto {

    private Long id;
    @Size(min = 3, max = 30)
    private String name;
    @Positive
    private int employeesCount;

    private Long organisation;

    private List<Long> users;


    public DepartmentModelDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(int employeesCount) {
        this.employeesCount = employeesCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public List<Long> getUsers() {
        return users;
    }

    public void setUsers(List<Long> users) {
        this.users = users;
    }

    public Long getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Long organisation) {
        this.organisation = organisation;
    }

}
