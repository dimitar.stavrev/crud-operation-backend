package com.example.demo.web;

import com.example.demo.service.OrganisationService;
import com.example.demo.web.model.DepartmentModelDto;
import com.example.demo.web.model.OrganisationModelDto;
import com.example.demo.web.model.RoleModelDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController

public class OrganisationControllerImpl implements OrganisationController{

    private final OrganisationService organisationService;

    public OrganisationControllerImpl(OrganisationService organisationService) {
        this.organisationService = organisationService;
    }

    @Override
    public ResponseEntity<OrganisationModelDto> getOrganisationByName(String name) {
        OrganisationModelDto organisation = organisationService.findByName(name);
        if (organisation == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(organisation, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<OrganisationModelDto> getOrganisationById(Long id) {
        OrganisationModelDto organisation = organisationService.findById(id);
        if (organisation == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(organisation, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<OrganisationModelDto>> getAllOrganisations() {
        return new ResponseEntity<>(organisationService.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> createOrganisation(@Valid @RequestBody OrganisationModelDto organisationModelDto) {

        OrganisationModelDto organisationDto = organisationService.findByName(organisationModelDto.getName());

        if (organisationDto != null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        organisationService.create(organisationModelDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<OrganisationModelDto> updateOrganisationById(Long id, OrganisationModelDto organisationModelDto) {

        return ResponseEntity.ok(organisationService.update(id, organisationModelDto));
    }

    @Override
    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            organisationService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
