package com.example.demo.web.model;

import javax.validation.constraints.Size;
import java.util.List;

public class OrganisationModelDto {

    private Long id;
    @Size(min = 3, max = 30)
    private String name;
    @Size(min = 5, max = 50)
    private String address;

    private List<Long> departments;            //  <DepartmentModelDto>


    public OrganisationModelDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<Long> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Long> departments) {
        this.departments = departments;
    }


}
