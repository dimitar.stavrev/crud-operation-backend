package com.example.demo.web;

import com.example.demo.service.RoleService;
import com.example.demo.web.model.RoleModelDto;
import com.example.demo.web.model.UserModelDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

public class RoleControllerImpl implements RoleController {

    private final RoleService roleService;

    public RoleControllerImpl(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public ResponseEntity<List<RoleModelDto>> getAllRoles() {

        return new ResponseEntity<>(roleService.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RoleModelDto> getRoleById(Long id) {
        RoleModelDto role = roleService.findById(id);
        if (role == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> createRoles(RoleModelDto roleModelDto) {

        RoleModelDto role = roleService.findByName(roleModelDto.getName());
        if (role != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        roleService.create(roleModelDto);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<RoleModelDto> updateRoleById(Long id, RoleModelDto roleModelDto) {

        return ResponseEntity.ok(roleService.update(id, roleModelDto));
    }

    @Override
    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            roleService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
