package com.example.demo.web;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.service.DepartmentService;
import com.example.demo.web.model.DepartmentModelDto;
import com.example.demo.web.model.OrganisationModelDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;

@RestController

public class DepartmentControllerImpl implements DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentControllerImpl(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Override
    public ResponseEntity<DepartmentModelDto> getDepartmentByName(String name) {
        DepartmentModelDto department = departmentService.findByName(name);

        if (department == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DepartmentModelDto>> getAllDepartments() {
        return new ResponseEntity<>(departmentService.findAll(), HttpStatus.OK);
    }
    @Override
    public ResponseEntity<DepartmentModelDto> getDepartmentById(Long id) {
        DepartmentModelDto department = departmentService.findById(id);
        if (department == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DepartmentModelDto>> search(DepartmentFilter filter) {
        return new ResponseEntity<>(departmentService.findByDepartmentNameAndEmployeesCount(filter),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> createDepartment(@Valid @RequestBody DepartmentModelDto departmentModelDto) {

        DepartmentModelDto departmentDto = departmentService.findByName(departmentModelDto.getName());

        if (departmentDto != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        departmentService.create(departmentModelDto);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DepartmentModelDto> updateDepartmentById(Long id, DepartmentModelDto departmentModelDto) {
        return ResponseEntity.ok(departmentService.update(id, departmentModelDto));
    }

    @Override
    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            departmentService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
