package com.example.demo.web;

import com.example.demo.web.model.RoleModelDto;
import com.example.demo.web.model.UserModelDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/role",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)

public interface RoleController {


    @GetMapping(value = "/readAll")
    ResponseEntity<List<RoleModelDto>> getAllRoles();

    @GetMapping(value = "/getId/{id}")
    ResponseEntity<RoleModelDto> getRoleById(@PathVariable Long id);

    @PostMapping(value = "/create")
    ResponseEntity<Void> createRoles(@Valid @RequestBody RoleModelDto roleModelDto);

    @PutMapping("/update/{id}")
    ResponseEntity<RoleModelDto> updateRoleById(@PathVariable("id") Long id, @RequestBody RoleModelDto roleModelDto);

    @DeleteMapping(value = "/delete/{id}")
    ResponseEntity<HttpStatus> deleteById(@PathVariable("id") Long id);


}
