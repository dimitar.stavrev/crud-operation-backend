package com.example.demo.web;

import com.example.demo.web.model.DepartmentModelDto;
import com.example.demo.web.model.OrganisationModelDto;
import com.example.demo.web.model.RoleModelDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/organisation",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)

public interface OrganisationController {


    @GetMapping(value = "/read")
    ResponseEntity<OrganisationModelDto> getOrganisationByName(@RequestParam String name);

    @GetMapping(value = "/getId/{id}")
    ResponseEntity<OrganisationModelDto> getOrganisationById(@PathVariable Long id);

    @GetMapping(value = "/readAll")
    ResponseEntity<List<OrganisationModelDto>> getAllOrganisations();
    @PostMapping(value = "/create")
    ResponseEntity<Void> createOrganisation(@Valid @RequestBody OrganisationModelDto organisationModelDto);

    @PutMapping("/update/{id}")
    ResponseEntity<OrganisationModelDto> updateOrganisationById(@PathVariable("id") Long id, @RequestBody OrganisationModelDto organisationModelDto);

    @DeleteMapping("/delete/{id}")
    ResponseEntity<HttpStatus> deleteById(@PathVariable("id") Long id);


}
