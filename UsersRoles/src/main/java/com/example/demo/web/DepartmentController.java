package com.example.demo.web;

import com.example.demo.filter.DepartmentFilter;
import com.example.demo.web.model.DepartmentModelDto;
import com.example.demo.web.model.OrganisationModelDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/department",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)

public interface DepartmentController {


    @GetMapping(value = "/read")
    ResponseEntity<DepartmentModelDto> getDepartmentByName(@RequestParam String name);

    @GetMapping(value = "/getId/{id}")
    ResponseEntity<DepartmentModelDto> getDepartmentById(@PathVariable Long id);

    @GetMapping(value = "/readAll")
    ResponseEntity<List<DepartmentModelDto>> getAllDepartments();

    @PostMapping(value = "/search")
    ResponseEntity<List<DepartmentModelDto>> search(@RequestBody DepartmentFilter filter);

    @PostMapping("/create")
    ResponseEntity<Void> createDepartment(@Valid @RequestBody DepartmentModelDto departmentModelDto);

    @PutMapping("/update/{id}")
    ResponseEntity<DepartmentModelDto> updateDepartmentById( @PathVariable("id") Long id, @RequestBody DepartmentModelDto departmentModelDto);

    @DeleteMapping("/delete/{id}")
    ResponseEntity<HttpStatus> deleteById(@PathVariable("id") Long id);


}
