package com.example.demo.web;

import com.example.demo.filter.UserFilter;
import com.example.demo.model.User;
import com.example.demo.web.model.UserModelDto;
import com.example.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RestController
public class UserControllerImpl implements UserController {

    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<UserModelDto> getUserByUsername(@RequestParam String username) {

        UserModelDto user = userService.findByUsername(username);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<UserModelDto> getUserById(Long id) {
        UserModelDto user = userService.findById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<List<UserModelDto>> getAllUsers() {

        return new ResponseEntity<>(userService.findAll(),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> createUser(@Valid @RequestBody UserModelDto userModelDto) {

        UserModelDto usernameExists = userService.findByUsername(userModelDto.getUsername());

        if (usernameExists != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userService.create(userModelDto);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<UserModelDto>> searchByUsernameAndAge(UserFilter filter) {
        return new ResponseEntity<>(userService.findByUsernameAndAge(filter),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserModelDto> updateUserById(Long id, UserModelDto userModelDto) {

        return ResponseEntity.ok(userService.update(id, userModelDto));
    }

    @Override
    public ResponseEntity<HttpStatus> addUserRoles(Long userId, Long roleId) {
        try {
            userService.addUserRoles(userId, roleId);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<HttpStatus> removeUserRoles(Long userId, Long roleId) {
        try {
            userService.removeUserRoles(userId, roleId);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Override
    public ResponseEntity<HttpStatus> deleteUserById(@PathVariable("id") Long id) {
        try {
            userService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
